import in_class_practice.LanguageBasics;

public class Main {
    public static void main(String[] args) {

        double a, b, c, d, x1, x2;

        a = 4;
        b = 4;
        c = 1;
        d = Math.pow(b, 2) - (4 * a * c);

        if (d > 0) {
            x1 = ((-1) * b + Math.sqrt(d)) / (2 * a);
            x2 = ((-1) * b - Math.sqrt(d)) / (2 * a);
            System.out.println("x1=" + x1 + "; x2=" + x2);
        } else if (d == 0) {
            x1 = (-1) * b / (2 * a);
            x2 = x1;
            System.out.println("x1, x2=" + x1);
        } else
            System.out.println("Дискриминант меньше нуля, решения нет");


    }
}
