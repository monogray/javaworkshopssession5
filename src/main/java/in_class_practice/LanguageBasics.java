package in_class_practice;

public class LanguageBasics {

    /*
    Заданы 2 стороны прямоугольника: 50мм и 80мм.
    Необходимо рассчитать площадь прямоугольника. И вывести на экран.
     */
    public void rectangleArea_1() {
        int a = 50;
        int b = 80;

        int s = a * b;

        System.out.println("Площадь прямоугольника равна: " + s + " мм^2");
    }

    /*
    Заданы 2 стороны прямоугольника: 50.30мм и 80.865мм.
    Необходимо рассчитать площадь прямоугольника.
     */
    public void rectangleArea_2() {
        double a = 50.30;
        double b = 80.865;

        double s = a * b;

        System.out.println("Площадь прямоугольника равна: " + s + " мм^2");
    }

    /*
    Заданы 3 стороны треугольника: 100мм, 70мм, 60мм. Необходимо рассчитать площадь треугольника (по формуле Герона).
    S = √p(p - a)(p - b)(p - c)
    где S - площадь треугольника,
    a, b, c - длины сторон треугольника,
    http://prntscr.com/jed2ai - полупериметр треугольника.
    */
    public void triangleArea() {
        double a = 100;
        double b = 70;
        double c = 60;

        double p = (a + b + c) / 2;
        double s = Math.sqrt(p * (p - a) * (p - b) * (p - c));

        System.out.println("Площадь треугольника равна: " + s + " мм^2");
    }

    /*
    Есть 2 переменные:
    int x = 7;
    int y = 10;
    Необходимо поменять местами их значения. Решить необходимо 2мя разными способами.
    */
    public void switchTwoVariables_1() {
        int x = 7;
        int y = 10;

        x = y - x;
        y = y - x;
        x = x + y;

        System.out.println("X = " + x);
        System.out.println("Y = " + y);
    }

    public void switchTwoVariables_2() {
        int x = 7;
        int y = 10;

        int z = x;
        x = y;
        y = z;

        System.out.println("X = " + x);
        System.out.println("Y = " + y);
    }

    /*
    В банке существует счет на сумму 20 000. Годовой процент 10% (начисление на счет происходит каждый месяц).
    Процент рассчитывается от текущей суммы на счету.
    Необходимо рассчитать сколько будет на счету: спустя 3 месяца, спустя 6 месяца, спустя 2 года.

    Комментрий: решить без использования циклов (будетрасммотренно в последующих лекциях).
    */
    public void annualPercentage() {
        double summ = 20000;
        double yearPercent = 10;
        double monthPercent = yearPercent / 12;

        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;

        System.out.println("Сумма на счету спустя 3 месяца = " + summ);


        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;

        System.out.println("Сумма на счету спустя 6 месяца = " + summ);


        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;
        summ = summ * monthPercent / 100 + summ;

        System.out.println("Сумма на счету спустя 24 месяца = " + summ);
    }

}
